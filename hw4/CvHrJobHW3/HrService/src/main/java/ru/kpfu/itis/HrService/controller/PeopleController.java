package ru.kpfu.itis.HrService.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ilya Ivlev
 */

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/peoples")
public class PeopleController {

}
